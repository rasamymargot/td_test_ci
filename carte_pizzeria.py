class CartePizzeria:
    listeDePizzas : list = []

    def __init__(self) -> None:
        pass

    def is_empty(self):
        return self.listeDePizzas.__len__() == 0

    def nb_pizzas(self):
        return len(self.listeDePizzas)

    def add_pizza(self, pizza):
        self.listeDePizzas.append(pizza)

    # def remove_pizza(self, name):
    #     hasPizzaToRemove = False

    #     for i in range(len(self.listeDePizzas)):
    #         if self.listeDePizzas[i].name == name:
    #             hasPizzaToRemove = True
    #             del self.listeDePizzas[i]

    #     if hasPizzaToRemove == False:
    #         raise ExceptionNoPizzaFound

class ExceptionNoPizzaFound(Exception):
    print("Raised when the pizza to remove is not found in the pizzas list.")
    pass