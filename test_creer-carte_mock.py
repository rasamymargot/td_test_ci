from mock import Mock, patch
from carte_pizzeria import CartePizzeria


def test_is_not_empty():
    maPizza = Mock(name= 'Bolognaise', ingredients= ['tomates', 'viande'], prix= 14)  
    maCartePizzeria = CartePizzeria()
    maCartePizzeria.listeDePizzas = [maPizza]
    res = maCartePizzeria.is_empty()
    expected_res = False
    assert res == expected_res

# def test_is_empty():
#     maCartePizzeriaVide = CartePizzeria()
#     assert maCartePizzeriaVide.is_empty()

def test_nb_pizzas():
    mesPizzas = [
        Mock(name= 'Bolognaise', ingredients= ['tomates', 'viande'], prix= 14),
        Mock(name= 'Bolognaise', ingredients= ['tomates', 'viande'], prix= 14),
        Mock(name= 'Bolognaise', ingredients= ['tomates', 'viande'], prix= 14) 
    ] 
    maCartePizzeria = CartePizzeria()
    maCartePizzeria.listeDePizzas = mesPizzas
    assert maCartePizzeria.nb_pizzas() == mesPizzas.__len__()

def test_pizza_added():
    maPizzeria = CartePizzeria()
    maPizza = Mock(name= 'Carbonara', ingredients= ['tomates', 'viande'], prix= 14)
    maPizzeria.add_pizza(maPizza)
    # Get the last element of the pizza list
    maPizzaAjoutee = maPizzeria.listeDePizzas[-1]
    assert maPizzaAjoutee == maPizza

# def test_pizza_removed():
#     maPizzeria = CartePizzeria()
#     maPizza = Mock(name= 'Carbonara', ingredients= ['tomates', 'viande'], prix= 14)
#     maPizzeria.listeDePizzas.append(maPizza)

#     maPizzeria.remove_pizza('Carbonara')
    
#     stillContainsUnremovedPizza = maPizzeria.listeDePizzas.__contains__(maPizza)
#     assert stillContainsUnremovedPizza == False
